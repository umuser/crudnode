var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
	name:String,
	username: {type:String, required:true, unique:true},
	admin: Boolean,
	location: String,
	age: Number,
	created: Date
});

var Users = mongoose.model('Users', userSchema);

module.exports = Users;