'use strict';

var Hapi = require('hapi');
var mongoose = require('mongoose');
var Users = require('./Users');
var bodyParser = require('body-parser');


var db = 'mongodb://localhost:27017/crudapp';
mongoose.connect(db, {
	keepAlive: true,
  reconnectTries: Number.MAX_VALUE,
  useMongoClient: true
});

var server = new Hapi.Server();

server.connection({
	port:3500,
	host:'127.0.0.1'
});

server.route({
	method:'GET',
	path:'/',
	handler:function(req, res){
		console.log("Get User");
		Users.find({}).exec(function(err,user){
			if(err)
				res("ERROR");
			res(user);
		});
		
	}
});

server.route({
	method:'POST',
	path:'/',
	handler:function(req, res){
		console.log("Add User");

		var newUser = new Users();

		newUser.name = req.payload.name;
		newUser.username = req.payload.username;
		newUser.admin = req.payload.admin;
		newUser.location = req.payload.location;
		newUser.age = req.payload.age;

		newUser.save(function(err, user){
			if (err){
				console.log(err);
				return res(err);
			}
			res(user);
		});
	}
});

server.route({
	method:'PUT',
	path:'/{id}',
	handler:function(req, res){
		console.log("Update User");
		var id = req.params.id;
		Users.findOneAndUpdate({
			_id:id
		},
		{$set:{name:req.payload.name}},
		function(err, user){
			if (err){
				console.log(err);
				return res(err);
			}
			res(user);
		});
	}
});

server.route({
	method:'DELETE',
	path:'/{id}',
	handler:function(req, res){
		console.log("Delete User");
		Users.findOneAndRemove({
		_id:req.params.id
		},function(err,users){
			if(err)
				return res("error deleting book");
			res(users);
	});
	}
});


server.start(function(err){
	if(err){
		throw err;
	}

	console.log("Server is running at", server.info.uri);
});